package com.farmlei.facereg.runner;

import com.farmlei.facereg.modules.face.utils.ArcsoftUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component//被spring容器管理
@Order(1)//如果多个自定义ApplicationRunner，用来标明执行顺序
public class MyApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        System.out.println("-------------->" + "项目启动，当前时间：" + new Date());

        try {
            System.out.println("初始化虹软人脸识别离线SDK");
            ArcsoftUtil.init();
            System.out.println("初始化虹软人脸识别离线SDK成功");
        } catch (LinkageError error) {
            System.out.println("error,初始化虹软人脸识别离线SDK失败：" + error.getMessage());
        } catch (Exception e) {
            System.out.println("exception,初始化虹软人脸识别离线SDK失败：" + e.getMessage());
        }
    }


}
