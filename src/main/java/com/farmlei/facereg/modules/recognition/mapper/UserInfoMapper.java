package com.farmlei.facereg.modules.recognition.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmlei.facereg.modules.recognition.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 人员信息 Mapper 接口
 * </p>
 *
 * @author farmlei
 * @since 2018-10-04
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
    /**
     * 分页查询用户列表
     * @return 分页对象
     */
    Page<UserInfo> selectPageVo(Page page);
}
