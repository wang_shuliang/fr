package com.farmlei.facereg.modules.recognition.service;

import com.farmlei.facereg.modules.recognition.entity.UserFaceFeature;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 人脸特征信息 服务类
 * </p>
 *
 * @author farmlei
 * @since 2018-10-05
 */
public interface IUserFaceFeatureService extends IService<UserFaceFeature> {
    /**
     * 保存人脸信息
     * @param userId 用户ID
     * @param faceImg 人脸图片路径
     * @return
     */
    void saveFaceFeature(Long userId, String faceImg) throws Exception;
}
