/**
 * Created by Administrator on 2017/10/9.
 */
var prefix = "/";
$(function() {
    loadData();

    $("#refreshBtn").click(function(){
        reLoad();
    });

    /**
     * 新增按钮
     */
    $("#addBtn").click(function(){
        $("#name").val("");
        $("#show_faceImg").empty();
        layer.open({
            type: 1,
            offset: '50px',
            skin: 'layui-layer-molv',
            title: "新增用户",
            area: ['300px', '450px'],
            shade: 0.5,
            shadeClose: false,
            content: $("#addLayer"),
            btn: ['保存', '取消'],
            yes: function (index) {
                //点击确定
                var name = $("#name").val();
                var faceImg = $("#faceImg").val();

                //新增用户
                $.ajax({
                    type: "POST",
                    url: prefix + "addUser",
                    data: {
                        name: name,
                        faceImg: faceImg
                    },
                    success: function(data){
                        var result = data.result;
                        if (result && result === "success") {
                            layer.msg("保存成功", {
                                icon: 1,
                                time: 1500
                            });
                            layer.close(index);
                            reLoad(true);
                        } else {
                            if (data.error) {
                                layer.msg(data.error, {
                                    icon: 2,
                                    time: 1500
                                });
                            } else {
                                layer.msg("系统异常", {
                                    icon: 2,
                                    time: 1500
                                });
                            }


                        }
                    }
                });
            },
            btn2: function(index) {
                layer.close(index);
            }
        });
    });

    /**
     * 人脸识别按钮
     */
    $("#recognizeBtn").click(function(){
        layer.open({
            type: 1,
            offset: '50px',
            skin: 'layui-layer-molv',
            title: "人脸识别",
            area: ['300px', '450px'],
            shade: 0.5,
            shadeClose: false,
            content: $("#recognizeLayer"),
            btn: ['校验', '取消'],
            yes: function (index) {
                var recognizeImg = $("#recognizeImg").val();

                //新增用户
                $.ajax({
                    type: "POST",
                    url: prefix + "recognizeUser",
                    data: {
                        recognizeImg: recognizeImg
                    },
                    success: function(data){
                        var result = data.result;
                        if (result && result === "success") {
                            layer.msg("匹配到用户：" + data.name, {
                                icon: 1,
                                time: 1500
                            });
                        } else {
                            if (data.error) {
                                layer.msg(data.error, {
                                    icon: 2,
                                    time: 1500
                                });
                            } else {
                                layer.msg("系统异常", {
                                    icon: 2,
                                    time: 1500
                                });
                            }


                        }
                    }
                });
            },
            btn2: function(index) {
                layer.close(index);
            }
        });
    });
});

function loadData() {
    $('#listTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "listUser", // 服务器数据的加载地址
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                // queryParamsType : "limit",
                // //设置为limit则会发送符合RESTFull格式的参数
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParams : function(params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageSize: params.limit,
                        pageNo:params.offset
                    };
                },
                columns : [
                    {
                        field : 'id',
                        title : '编号'
                    },
                    {
                        field : 'name',
                        title : '姓名'
                    },
                    {
                        field : 'faceImg',
                        title : '人脸照片'
                    }]
            });
}
function reLoad(fromFirstPage) {
    if (fromFirstPage) {
        $("#listTable").bootstrapTable("refreshOptions",{pageNumber:1});
        $('#listTable').bootstrapTable('refresh');
    } else {
        $('#listTable').bootstrapTable('refresh');
    }
}

//当AJAX上传图片操作
function ajaxFileUpload(inputId) {
    var index = layer.load(1, {
        shade: [0.1,'#fff'] //0.1透明度的白色背景
    });
    $.ajaxFileUpload
    (
        {
            url: prefix + 'ajaxImageUpload', //用于文件上传的服务器端请求地址up参数标记此次是上传操作还是裁剪操作
            secureuri: false, //一般设置为false，是否安全上传
            fileElementId: inputId + "File", //文件上传控件的id属性  <input type="file" id="file" name="file" />
            dataType: 'json', //返回值类型 一般设置为json 期望服务器传回的数据类型
            success: function (data, status)  //服务器成功响应处理函数
            {
                var result = data.result;
                if (result && result === "success") {
                    var imagePath = data.img_path;
                    var imageUrl = data.img_url;
                    $("#show_" + inputId).html("<p><img style='max-height: 200px;max-width: 200px;' src='"+imageUrl+"'>" +
                        "<input type='hidden' id='"+inputId+"' name='"+inputId+"' value='"+imagePath+"'></p>"
                    );
                } else {
                    if (data.error) {
                        layer.msg(data.error, {
                            icon: 2,
                            time: 1500
                        });
                    } else {
                        layer.msg("系统异常", {
                            icon: 2,
                            time: 1500
                        });
                    }
                }
                $("#" + inputId + "File").val("");
                layer.close(index);
            },
            error: function (data, status, e)//服务器响应失败处理函数
            {
                layer.msg("系统错误：请检查图片类型及大小是否符合要求", {
                    icon: 2,
                    time: 1500
                });
                $("#" + inputId + "File").val("");
                layer.close(index);
            }
        }
    );
    return false;
}